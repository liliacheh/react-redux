import { cartQuantityTypes } from "../types";
let storedСartQuantity;
try {
  storedСartQuantity = JSON.parse(localStorage.getItem('cartQuantity'));
} catch (error) {
  console.error('Error parsing cartQuantity from LocalStorage');
  storedСartQuantity = null;
}
const initialState = storedСartQuantity && typeof storedСartQuantity === 'number'
  ? storedСartQuantity : null;

export function cartQuantityReducer (state = initialState, action) {
    switch (action.type){
    case cartQuantityTypes.GET_CART_QUANTITY:
        return state
    case cartQuantityTypes.SET_CART_QUANTITY:
        return action.payload
    default: return state
    }
}
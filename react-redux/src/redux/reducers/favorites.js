import { favoritesTypes } from "../types";
let storedFavorites;
try {
  storedFavorites = JSON.parse(localStorage.getItem('favorites'));
} catch (error) {
  console.error('Error parsing favorites from LocalStorage');
  storedFavorites = [];
}
const initialState = storedFavorites && Array.isArray(storedFavorites) 
&& storedFavorites.every(item => typeof item === 'number')
  ? storedFavorites : [];

export function favoritesReducer(state = initialState, action) {
    switch (action.type) {
        case favoritesTypes.GET_FAVORITES:
            return action.payload
        case favoritesTypes.SET_FAVORITES:
            return action.payload
        default: 
            return state
    }
    
}
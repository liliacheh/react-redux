import { cartTypes } from "../types";
let storedCart;
try {
  storedCart = JSON.parse(localStorage.getItem('cart'));
} catch (error) {
  console.error('Error parsing cart from LocalStorage');
  storedCart = [];
}
const initialState = storedCart && Array.isArray(storedCart) 
&& storedCart.every(item => typeof item === 'object')
  ? storedCart : [];

export const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case cartTypes.GET_CART:
      return action.payload;
    case cartTypes.ADD_TO_CART:
      return [...state, action.payload];
    case cartTypes.INCREASE_ITEM_NUM_IN_CART:
    case cartTypes.DECREASE_ITEM_NUM_IN_CART:
      return state.map(item => {
        if (item.id === action.payload.id) {
          return action.payload;
        }
        return item;
      });
    case cartTypes.REMOVE_FROM_CART:
      return action.payload
    default:
      return state;
  }
};

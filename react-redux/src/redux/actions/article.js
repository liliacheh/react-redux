import { articleTypes } from "../types";

export function getArticle(){
    return {
        type: articleTypes.GET_ARTICLE,
    }
}

export function setArt(article){
    return {
        type: articleTypes.SET_ARTICLE,
        payload: article
    }
}
import { favoritesTypes } from "../types";

export function getFav(favorites){
    return {
        type: favoritesTypes.GET_FAVORITES,
        payload: favorites
    }
}
export function getFavorites() {
    let favorites = localStorage.getItem('favorites');
  
    try {
      favorites = JSON.parse(favorites);
    } catch (error) {
      console.error('Favorites must be valid JSON');
      return dispatch => dispatch(getFav([]));
    }
  
    if (favorites && Array.isArray(favorites) && favorites.every(item => typeof item === 'number')) {
      return dispatch => dispatch(getFav(favorites));
    } else {
      console.error('Favorites must be an array of numbers');
      return dispatch => dispatch(getFav([]));
    }
  }
  

export function setFav(favorites){
    return {
        type: favoritesTypes.SET_FAVORITES,
        payload: favorites
    }
}

export function setFavorites(favorites, article){
    if (!favorites.includes(article)) {
       const newFavorites = [...favorites, article]
     localStorage.setItem('favorites', JSON.stringify(newFavorites))
       return dispatch => dispatch(setFav(newFavorites))
    } else {
      const newFavorites = favorites.filter(fav => fav !== article)
      setFavorites(newFavorites, article)
      localStorage.setItem('favorites', JSON.stringify(newFavorites))
      return dispatch => dispatch(setFav(newFavorites))
    } 
}

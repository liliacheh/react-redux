import { productsTypes } from "../types"

export function getProductsRequest(){
    return{
        type: productsTypes.GET_PRODUCTS_REQUESTED,
    }
}
export function getProductsSuccess(products){
    return{
        type: productsTypes.GET_PRODUCTS_SUCCESS,
        payload: products
    }
}
export function getProductsError(error){
    return{
        type: productsTypes.GET_PRODUCTS_ERROR,
        payload: error
    }
}

export const getProductsAsync = () => {
    return async function(dispatch) {
      try {
        dispatch(getProductsRequest());
        const products = await fetch("/products.json")
          .then(res => res.json());
        dispatch(getProductsSuccess(products));
      } catch (error) {
        dispatch(getProductsError(error));
      }
    };
  };
  
  

import { cartQuantityTypes } from "../types";

export function getCartQuantity(){
    return {
        type: cartQuantityTypes.GET_CART_QUANTITY
    }
}
export function setCartNum(totalQuantity){
    return {
        type: cartQuantityTypes.SET_CART_QUANTITY,
        payload: totalQuantity
    }
}
export function setCartQuantity(totalQuantity){
    if (typeof totalQuantity === 'number') {
    localStorage.setItem('cartQuantity', JSON.stringify(totalQuantity));
    return dispatch => dispatch(setCartNum(totalQuantity))
    } else {
        localStorage.setItem('cartQuantity', JSON.stringify(null));
        return dispatch => dispatch(setCartNum(null))
    }
    
}
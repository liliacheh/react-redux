import ProductList from "../../components/productList";
import { Loader } from "../../components/loader";
import {useDispatch, useSelector} from 'react-redux'
import { useEffect } from "react";
import { getProductsAsync } from "../../redux/actions/products";
import { NotFound } from "../NotFound/NotFound";
import { errorText } from "../NotFound/errorText";


export default function Home (){

const dispatch = useDispatch();
const { products, loading, error } = useSelector(state => state.products);

useEffect(() => {
  dispatch(getProductsAsync()); 
}, [dispatch]);
  
if (error) return (<NotFound text={errorText}/>)

      return(
     <>
     {!loading ?
     <ProductList 
     products={products}
     type='buy'
     />:
     <Loader/>}
     </>
      )
  };



import style from "./notFound.module.scss"
import Header from "../../components/header";

export function NotFound(props) {
     const favorites = JSON.parse(localStorage.getItem('favorites'))
     const cart = JSON.parse(localStorage.getItem('cartQuantity'))

    return (
        <>
        <Header 
        favorites={favorites.length}
        cart={cart}/>
        <div className={style.text}>
        <h1 className={style.title}>Oops!</h1>
        <p className={style.par}>Sorry, something went wrong </p>
        <p className={style.par}>{props.text}</p>
        </div>

        </>
    )
}

import { Outlet } from 'react-router-dom';
import Header from '../../components/header';
import { useEffect } from 'react';
import Modal from '../../components/modal';
import { useDispatch, useSelector } from 'react-redux';
import { setCartQuantity } from '../../redux/actions/cartQuantity';

export function Layout() {
  const dispatch = useDispatch();
  const modalType = useSelector((state) => state.modalType.modalType);
  const cart = useSelector((state) => state.cart);
  const totalQuantity = cart.reduce((sum, current) => sum + current.quantity, 0);

  useEffect(() => {
    dispatch(setCartQuantity(totalQuantity));
  }, [totalQuantity, dispatch]);

  return (
    <>
      {modalType && <Modal />}
      <Header />
      <Outlet />
    </>
  );
}
